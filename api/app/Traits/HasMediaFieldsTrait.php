<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Facades\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait HasMediaFieldsTrait
{
    use InteractsWithMedia;

    public function setAttribute($key, $value)
    {
        if (!$this->isMediaAttribute($key)) {
            return parent::setAttribute($key, $value);
        }

        switch ($this->mediaFields[$key]) {
            case 'image':
                $this->storeBase64Image($key, $value);
                break;
            case 'repeatable_image':
                $this->storeBase64Images($key, $value);
                break;
            case 'file':
                $this->storeUploadedFile($key, $value);
                break;
            case 'images':
            case 'files':
                $this->storeUploadedFiles($key, $value);
                break;
        }

        return $this;
    }

    public function getAttribute($key)
    {
        if (!$this->isMediaAttribute($key)) {
            return parent::getAttribute($key);
        }

        $value = null;
        switch ($this->mediaFields[$key]) {
            case 'image':
            case 'file':
                $value = $this->getFileUrl($key);
                break;
            case 'repeatable_image':
                $value = $this->getRepeatableImageUrls($key);
                break;
            case 'images':
            case 'files':
                $value = $this->getFileUrls($key);
                break;
        }

        return $value;
    }

    protected function isMediaAttribute($key)
    {
        return in_array($key, array_keys($this->mediaFields), true);
    }

    public function getResponsiveImage($field)
    {
        $media = $this->getFirstMedia($field);

        if (!$media) {
            return null;
        }

        return $media->toHtml();
    }

    public function getResponsiveImages($field)
    {
        $media = $this->getMedia($field);
        $result = [];

        foreach ($media as $item) {
            $result[] = ['image' => $item->toHtml()];
        }
        return $result;
    }

    public function getOriginalImage($field)
    {
        $src = $this->{$field};
        if (!$src) {
            return null;
        }

        return '<img src="'. $src .'">';
    }

    protected function storeBase64Image($collection, $value)
    {
        if ($value === null) {
            $this->clearMediaCollection($collection);
        }
        else if (Str::startsWith($value, 'data:image')) {
            preg_match("/^data:image\/([a-zA-Z]*);base64/i", $value, $match);
            $extension = $match[1];

            $this->clearMediaCollection($collection);
            $this->addMediaFromBase64($value)
                ->usingFileName($this->getRandomFileName($value) . '.' . $extension)
                ->withResponsiveImages()
                ->toMediaCollection($collection);
        }
    }

    protected function storeBase64Images($collection, $value)
    {
        $images = json_decode($value);
        $media = $this->getMedia($collection);
        $excludedMedia = [];

        foreach ($media as $mediaItem) {
            $mediaUrl = $mediaItem->getFullUrl();
            foreach ($images as $item) {
                if ($item->image === $mediaUrl) {
                    $excludedMedia[] = $mediaItem;
                    break;
                }
            }
        }

        $this->clearMediaCollectionExcept($collection, $excludedMedia);

        foreach ($images as $item) {
            $base64Image = $item->image;
            if (Str::startsWith($base64Image, 'data:image')) {
                preg_match("/^data:image\/([a-zA-Z]*);base64/i", $base64Image, $match);
                $extension = $match[1];

                $this->addMediaFromBase64($base64Image)
                    ->usingFileName($this->getRandomFileName($value) . '.' . $extension)
                    ->withResponsiveImages()
                    ->toMediaCollection($collection);
            }
        }
    }

    protected function storeUploadedFile($collection, $value)
    {
        if ($value === null) {
            $this->clearMediaCollection($collection);
        }
        else if ($value instanceof \SplFileInfo && $value->getPath() !== '') {
            $this->clearMediaCollection($collection);
            $this->addMedia($value)
                ->preservingOriginal()
                ->usingName($this->getRandomFileName($value))
                ->withResponsiveImages()
                ->toMediaCollection($collection);
        }
    }

    protected function storeUploadedFiles($collection, $values)
    {
        $mediaToDelete = Request::get("clear_$collection");
        if ($mediaToDelete) {
            $excludedMedia = Media::whereNotIn('id', $mediaToDelete)->where('collection_name', $collection)->get();
            $this->clearMediaCollectionExcept($collection, $excludedMedia);
        }

        foreach ($values as $value) {
            if ($value instanceof \SplFileInfo && $value->getPath() !== '') {
                $this->addMedia($value)
                    ->preservingOriginal()
                    ->usingName($this->getRandomFileName($value))
                    ->withResponsiveImages()
                    ->toMediaCollection($collection);
            }
        }
    }

    protected function getFileUrl($collection)
    {
        return $this->getFirstMediaUrl($collection);
    }

    protected function getRepeatableImageUrls($collection)
    {
        $media = $this->getMedia($collection);

        $result = [];
        foreach ($media as $file) {
            $result[] = ['image' => $file->getFullUrl()];
        }

        return $result;
    }

    protected function getFileDownloadLink($collection)
    {
        $media = $this->getFirstMedia($collection);

        if (!$media) {
            return null;
        }

        return '<a href="' . $media->getFullUrl() . '" target="_blank">' . $media->file_name . '</a>';
    }

    protected function getFileUrls($collection)
    {
        $media = $this->getMedia($collection);

        $result = [];
        foreach ($media as $file) {
            $result[$file->id] = $file->getFullUrl();
        }

        return $result;
    }

    protected function getRandomFileName($value)
    {
        return md5($value.time());
    }
}
