<?php

namespace App\Http\Controllers;

use App\Http\Resources\TestResource;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;

class TestController extends Controller
{
    const Ld = 12;

    public function index(Request $request)
    {
        $sessionId = $request->getClientIp();
        $testCollection = TestResource::collection(Test::where('session_id', $sessionId)
            ->orderBy('created_at', 'DESC')->get());
        return response()->json([
            'data' => $testCollection,
        ]);
    }

    private function convertColorImageToGrayImage($data)
    {
        // TODO: convert color image into gray image
    }

    public function store(Request $request)
    {
        $rules = [ 'image' => 'required' ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'errors' => [
                    'image' => 'Необходимо загрузить изображение.'
                ]
            ], 403);
        }

        $extension = $request->file('image')->extension();
        if (in_array($extension, ['bmp', 'jpg', 'jpeg', 'png'])) {
            $image = $request->file('image');
            $test = Test::create([
                'session_id' => $request->getClientIp(),
            ]);
            $test->addMedia($image)->toMediaCollection('image');
            list($width, $height) = getimagesize(public_path(mb_substr($test->image, strlen(config("app.url")) + 1)));
            $tempImage = ($extension == 'jpeg' || $extension == 'jpg') ?
                imagecreatefromjpeg(public_path(mb_substr($test->image, strlen(config("app.url")) + 1))) : (
                    $extension == 'png' ?
                        imagecreatefrompng(public_path(mb_substr($test->image, strlen(config("app.url")) + 1))) :
                        imagecreatefrombmp(public_path(mb_substr($test->image, strlen(config("app.url")) + 1)))
                );

            $countPixels = 300 / $width;

            $inputData = [];
            $outputData = [];
            $proofSigma = [];

            $desSigma = [];
            $desSum = 0;
            $maxDes = -99999999;
            $checkArray = [];

            $wave = is_null($request->get('wave')) ? 0 : $request->get('wave');
            $noise = is_null($request->get('noise')) ? 0 : $request->get('noise');

            $outputContent = '';
            for ($i = 0; $i < $height; $i++) {
                for ($j = 0; $j < $width; $j++) {

                    $rgb = imagecolorat($tempImage, $j, $i);
                    $r = ($rgb >> 16) & 0xFF;
                    $g = ($rgb >> 8) & 0xFF;
                    $b = $rgb & 0xFF;

                    $sigma = $r;
                    $inputData[$i][$j] = $sigma;

                    $li = sqrt(pow(self::Ld, 2) + pow((($j + 1) * $countPixels), 2));

                    $rand = (rand(95, 105) / 100);
                    $ld = ($wave == true) && ((self::Ld * $rand) < $li) ? ($rand * self::Ld) : self::Ld;

                    $outputData[$i][$j] =
                        (238.73 * exp(-0.036 * $li) * ($ld * $ld) * $sigma) /
                        (pow($li, 4) * sqrt(pow($li, 2) - ($ld * $ld)));

                    $item = $noise == true ? (rand(95, 105) / 100) * $outputData[$i][$j] : $outputData[$i][$j];

                    $proofSigma[$i][$j] =
                        1 / 238.73 * exp(0.036 * $li) / pow(self::Ld, 2) *
                        $item * (pow($li, 4) * sqrt(pow($li, 2) - pow(self::Ld, 2)));

                    if (($wave + $noise) == 0) {
                        $proofSigma[$i][$j] = (int) round($proofSigma[$i][$j]);
                    }

                    $desSigma[$i][$j] = abs($inputData[$i][$j] - $proofSigma[$i][$j]);
                    $desSum += ($desSigma[$i][$j] * $desSigma[$i][$j]) / ($height * $width);
                    if ($desSigma[$i][$j] > $maxDes) {
                        $maxDes = $desSigma[$i][$j];
                    }

                    if ($proofSigma[$i][$j] != $inputData[$i][$j]) {
                        $checkArray[] = [$i, $j];
                    }

                    $outputContent .= "{$outputData[$i][$j]} ";
                }
                $outputContent .= "\n";
            }

            $tempInputFile = tempnam(sys_get_temp_dir(), "test_{$test->id}_input");
            file_put_contents($tempInputFile, json_encode($inputData));
            $test
                ->addMedia(new UploadedFile($tempInputFile, "test_{$test->id}_input.json"))
                ->toMediaCollection('input');

            $tempProofFile = tempnam(sys_get_temp_dir(), "test_{$test->id}_proof");
            file_put_contents($tempProofFile, json_encode($proofSigma));
            $test
                ->addMedia(new UploadedFile($tempProofFile, "test_{$test->id}_proof.json"))
                ->toMediaCollection('proof');

            $tempOutputFile = tempnam(sys_get_temp_dir(), "test_{$test->id}_output");
            file_put_contents($tempOutputFile, $outputContent);
            $test
                ->addMedia(new UploadedFile($tempOutputFile, "test_{$test->id}_output.txt"))
                ->toMediaCollection('output');

            $test->data = json_encode([
                'Условие' => $wave == true ? 'Шум расстояния' : ($noise == true ? 'Шум сигнала' : 'Без шума'),
                'Максимальная погрешность' => $maxDes,
                'Среднее квадратичное' => sqrt($desSum),
            ]);
            $test->save();

        } else {
            return response()->json([
                'errors' => [
                    'image' => 'Необходимо загрузить валидное изображение.'
                ]
            ], 403);
        }

        return $this->index($request);
    }

    private function generateFile($data)
    {
        // TODO: generate file data
    }

    public function destroy(Request $request, int $id)
    {
        $test = Test::find($id);
        $test->delete();
        return $this->index($request);
    }
}
