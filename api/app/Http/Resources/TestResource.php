<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'session_id' => $this->session_id,
            'data' => is_null($this->data) ? [] : json_decode($this->data),
            'input' => $this->input,
            'proof' => $this->proof,
            'output' => $this->output,
            'image' => $this->image,
        ];
    }
}
