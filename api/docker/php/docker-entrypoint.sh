#!/bin/sh
set -e

if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'php' ]; then
	PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-production"
	if [ "$APP_ENV" != 'prod' ]; then
		PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-development"
	fi
	ln -sf "$PHP_INI_RECOMMENDED" "$PHP_INI_DIR/php.ini"

	mkdir -p public/media public/uploads
	chmod -R 777 storage public/media public/uploads
#	setfacl -R -m u:www-data:rwX -m u:"$(whoami)":rwX storage
#	setfacl -dR -m u:www-data:rwX -m u:"$(whoami)":rwX storage

fi

exec docker-php-entrypoint "$@"
