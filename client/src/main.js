import Vue from 'vue';
import App from './App.vue';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify);
const opts = {};
let vuetify = new Vuetify(opts);

const moment = require('moment');
require('moment/locale/ru');

Vue.use(require('vue-moment'), {
  moment
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  vuetify
}).$mount('#app');
